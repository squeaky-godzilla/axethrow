using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody axe;
    public GameObject playerCam;
    // Boolean holdingAxe;
    // Boolean flyingAxe;
    // public int axeCount;
    public float axeHoldingOffsetFwd;
    public float axeHoldingOffsetUp;
    public float throwPowerFwd;
    public float throwPowerUp;
    public float throwSpin;
    public float maxThrowSpin;
    public float chargingScale;
    public float powerMagnitude;

    Boolean chargingThrow;
    float powerMplier;

    void Start()
    {
        // flyingAxe = false;
        // holdingAxe = true;
        chargingThrow = false;
    }

    // Update is called once per frame
    void Update()
    {
        // if (!holdingAxe) {
        //     Rigidbody axeClone;

        //     axeClone = Instantiate(axe, transform.position, transform.rotation);
        // }
        if (Input.GetMouseButtonDown(0)) {
            chargingThrow = true;
        }

        if (chargingThrow) {
            powerMplier += Time.deltaTime * chargingScale;
        }

        if (Input.GetMouseButtonUp(0)) {
            chargingThrow = false;
            if (powerMplier > 1) {
                powerMplier = 1;
            }

            Rigidbody axeClone;

            axeClone = Instantiate(axe, 
            playerCam.transform.position + playerCam.transform.forward * axeHoldingOffsetFwd + playerCam.transform.up * axeHoldingOffsetUp, 
            playerCam.transform.rotation
            );
            axeClone.maxAngularVelocity = maxThrowSpin;
            axeClone.AddForce(playerCam.transform.forward*throwPowerFwd*powerMplier*powerMagnitude + playerCam.transform.up*throwPowerUp*powerMplier*powerMagnitude, ForceMode.Force);
            axeClone.AddTorque(playerCam.transform.right*throwSpin*powerMplier*powerMagnitude, ForceMode.Impulse);
            powerMplier = 0; //reset power multiplier
        }


    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blade_sensor : MonoBehaviour
{
    public float stickingSpeedThreshold;
    float bladeSpeed;

    void Start()
    {
        // InvokeRepeating("TrackVelocity", 1.0f, 1.0f);
    }

    // void OnCollisionEnter(Collision collision)
    // {
    //     Debug.Log($"collision speed: {collision.relativeVelocity.magnitude}");
    // }
    
    void TrackVelocity() {
    if (gameObject.GetComponent<Rigidbody>().velocity.magnitude > 0)
        {
        Debug.Log($"blade velocity: {gameObject.GetComponent<Rigidbody>().velocity}");
    }
    }

    void OnTriggerEnter(Collider other)
    {
        // bladeSpeed = gameObject.GetComponent<Rigidbody>().velocity.magnitude;
        // bladeSpeed = gameObject.GetComponent<Rigidbody>().inertiaTensor.magnitude;

        // if (other.tag == "speedCheck") {
        //     Debug.Log($"velocity_speedcheck: {gameObject.GetComponentInParent<Rigidbody>().velocity}");
        //     Debug.Log($"angular_velocity_speedcheck: {gameObject.GetComponentInParent<Rigidbody>().angularVelocity}");
        //     Debug.Log($"inertia_tensor_speedcheck: {gameObject.GetComponentInParent<Rigidbody>().inertiaTensor}");
            
        // }
        if (other.tag == "axeSticks") {
            // gameObject.GetComponentInParent<Rigidbody>().isKinematic = true;
            Destroy (gameObject.GetComponentInParent<Rigidbody>());
            gameObject.transform.parent.gameObject.transform.parent = other.transform.parent;
        }
    }

}
